<?php

namespace Drupal\role_expire\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Take values from role_expire table.
 *
 * @MigrateSource(
 *   id = "role_expire",
 *   source_module = "role_expire",
 * )
 */
class RoleExpire extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('role_expire', 're')
      ->fields('re', [
          'uid',
          'rid',
          'expiry_timestamp',
        ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'uid' => $this->t('User ID' ),
      'rid'   => $this->t('Role IDs' ),
      'expiry_timestamp'    => $this->t('Expiry Datetime'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'uid' => [
        'type' => 'integer',
        'alias' => 're',
      ],
    ];
  }
}
